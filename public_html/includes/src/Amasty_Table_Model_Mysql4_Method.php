<?php
/**
 * @copyright   Copyright (c) 2009-2012 Magento King(http://www.mamgentoking.com.br)
 */ 
class Amasty_Table_Model_Mysql4_Method extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('amtable/method', 'method_id');
    }          
}